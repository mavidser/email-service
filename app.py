from flask import Flask, request
from urlparse import urlparse
import subprocess

app = Flask(__name__)

@app.route('/')
def hello_world():
    return '''
        Start: Starting a web-browser process<br>
        Stop: Killing the web-browser process<br>
        Cleanup: Delete all the browsing session information such as history, cache, cookies, downloads, saved passwords, etc.  
'''

@app.route('/start')
def start():
    url = request.args['url']
    parse_result = urlparse(url)
    if not (parse_result.scheme and parse_result.netloc):
        return 'Give a valid url along with the scheme'
    p = subprocess.Popen(["adb shell am start -n com.android.chrome/com.google.android.apps.chrome.Main -d "+url], shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, err = p.communicate()
    if p.returncode==0:
        return 'Started.<br>'+output
    else:
        return 'Some error occurred.<br>'+err


@app.route('/stop')
def stop():
    p = subprocess.Popen("adb shell am force-stop com.android.chrome", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, err = p.communicate()
    if p.returncode==0:
        return 'Stopped.<br>'+output
    else:
        return 'Some error occurred.<br>'+err


@app.route('/cleanup')
def cleanup():
    p1 = subprocess.Popen("adb shell pm clear com.android.chrome", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    subprocess.Popen("adb shell 'echo \"chrome --disable-fre\" > /data/local/tmp/chrome-command-line'", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    subprocess.Popen("adb shell rm /sdcard/Download/*", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    subprocess.Popen("adb shell pm clear com.android.providers.downloads", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    
    output, err = p1.communicate()
    
    if p1.returncode==0:
        return 'Cleaned.<br>'+output
    else:
        return 'Some error occurred.<br>'+err

if __name__ == '__main__':
    app.run(debug=True)